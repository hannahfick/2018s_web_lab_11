-- Answers to Exercise 5 here
DROP TABLE IF EXISTS exercise_5;

CREATE TABLE IF NOT EXISTS exercise_5 (
  username   VARCHAR(64),
  first_name VARCHAR(64),
  last_name  VARCHAR(64),
  email VARCHAR(64),
  PRIMARY KEY (username)
);

INSERT INTO exercise_5 VALUES ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com');
INSERT INTO exercise_5 VALUES ('programmer2', 'Peter', 'Johnson', 'pj@someplace.com');
INSERT INTO exercise_5 VALUES ('peteL', 'Pete', 'Lowery', 'pl@someplace.com');
INSERT INTO exercise_5 VALUES ('bobbyP', 'Bobby', 'Peterson', 'bp@someplace.com');



SELECT * from exercise_5;