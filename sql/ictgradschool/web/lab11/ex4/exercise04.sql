-- Answers to Exercise 4 here
DROP TABLE IF EXISTS exercise_4;

CREATE TABLE IF NOT EXISTS exercise_4 (
  incrementedID INT NOT NULL AUTO_INCREMENT,
  title   VARCHAR(64),
  article TEXT,
  PRIMARY KEY (incrementedID)
);

INSERT INTO exercise_4 (title, article)
VALUES ('Lorem Ipsum', 'Lorem ipsum dolor sit amet, et putent labores conclusionemque his.Cu mea tale similique, perfecto recusabo platonem quo eu.At sit inimicus voluptaria, feugait dissentiet comprehensam ne vix.Ne per dicant putent invidunt, ea per partiendo repudiare.

Vis eu dicam patrioque mnesarchum.At cum inani inermis pericula, simul eripuit quo at.In qui discere torquatos, ut suscipit appareat eos.Tollit dolore eripuit vis ei, ea duo reque error essent.Ei sed vidit discere scripserit.Eam prompta invidunt expetendis at.

Te debet epicurei cum.Pro iisque molestiae similique te, nam tale complectitur ex.Brute nihil duo ea, et tation impetus vix.Consul virtute ad sed, an nec soluta constituam.Mutat graeco persius ex sed.

Duo an timeam abhorreant contentiones, ex ius fabulas ponderum inciderint.Denique periculis ex has.Cotidieque efficiantur ei usu.Etiam harum albucius usu at.Ex purto salutandi consequat est, eu has solum dicam.

Dolorum dolorem in mei, est graeco nostro cetero ad.Vel fugit legendos et, eam volumus praesent patrioque ut.Illum decore quo no, quidam dictas aeterno cu vix, pro no aeque mandamus.Amet quas nonumes vis id, alterum percipitur an nec.Eu accumsan in doctum mel, quando commune imperdiet id quo.Ex detracto apeirian per, sint fuisset consulatu cum at.');