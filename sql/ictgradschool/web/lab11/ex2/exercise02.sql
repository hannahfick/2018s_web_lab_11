-- Answers to Exercise 2 here
DROP TABLE IF EXISTS exercise_2;

CREATE TABLE IF NOT EXISTS exercise_2 (
  username   VARCHAR(64),
  first_name VARCHAR(64),
  last_name  VARCHAR(64),
  email VARCHAR(64)
  );


INSERT INTO exercise_2 VALUES ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com');
INSERT INTO exercise_2 VALUES ('programmer1', 'Peter', 'Johnson', 'pj@someplace.com');
INSERT INTO exercise_2 VALUES ('peteL', 'Pete', 'Lowery', 'pl@someplace.com');
INSERT INTO exercise_2 VALUES ('bobbyP', 'Bobby', 'Peterson', 'bp@someplace.com');;