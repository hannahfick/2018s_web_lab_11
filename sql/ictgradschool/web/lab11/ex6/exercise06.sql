-- Answers to Exercise 6 here

DROP TABLE IF EXISTS exercise_6;

CREATE TABLE IF NOT EXISTS exercise_6 (
  barcode    INT NOT NULL,
  director   VARCHAR(80),
  title      VARCHAR(80),
  cost       INT,
  borrowerID INT NOT NULL,
  PRIMARY KEY (barcode),
  FOREIGN KEY (borrowerID) REFERENCES exercise_3 (id)
);

INSERT INTO exercise_6 (barcode, director, title, cost, borrowerID)
VALUES (12345, 'Cameron Grout', 'Yay! Another day of Java!', 2, 2),
       (34567, 'Some fella', 'How many different ways can I explain the same thing?', 4, 6),
       (45678, 'Darcy Man', 'Dude, where is my code?', 6, 3),
       (56789, 'Yaz Yahtzee', 'Sup, ducky ducky', 2, 4),
       (67891, 'Steph Stephanie', 'Free beer?', 4, 1),
       (67893, 'Steph Stephanie', 'Free beer?', 4, 1);


SELECT *
FROM exercise_6;

