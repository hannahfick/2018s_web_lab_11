-- Answers to Exercise 9 here
SELECT * FROM exercise_3;

SELECT id,name,gender,year_born,joined FROM exercise_3;

SELECT title FROM exercise_4;
SELECT DISTINCT director FROM exercise_6;

SELECT title FROM exercise_6 WHERE cost <= 2;

SELECT username FROM exercise_5 ORDER BY username;

SELECT username FROM exercise_5 WHERE first_name LIKE 'Pete%';
SELECT username FROM exercise_5 WHERE first_name LIKE 'Pete%' OR last_name LIKE 'Pete%';