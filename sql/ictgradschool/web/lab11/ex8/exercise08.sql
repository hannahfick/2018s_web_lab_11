-- Answers to Exercise 8 here



DELETE FROM exercise_5 WHERE username = 'programmer1';

ALTER TABLE exercise_5 DROP COLUMN first_name;

DROP TABLE exercise_5;

UPDATE exercise_6
SET director = 'Someone Else'
WHERE barcode = 12345;

UPDATE exercise_6
SET barcode = 11111
WHERE barcode = 45678;